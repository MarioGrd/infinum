﻿namespace Infinum.ConsoleClient
{
    using Microsoft.AspNetCore.SignalR.Client;

    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public static class Program
    {
        private static async Task Main()
        {
            HubConnection connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/Contact")
                .Build();

            await connection.StartAsync();

            Console.WriteLine("Starting connection.");

            CancellationTokenSource cts = new CancellationTokenSource();

            Console.CancelKeyPress += async (sender, a) =>
            {
                a.Cancel = true;
                cts.Cancel();
                await connection.StopAsync();
            };

            connection.Closed += async (error) =>
            {
                Console.WriteLine("Connection closed with error: {0}", error);

                await connection.StopAsync();
            };

            connection.On<string>("contactChanged", (message) =>
            {
                Console.WriteLine("Message received.");
                Console.WriteLine(message);
            });

            Console.ReadLine();
        }
    }
}
