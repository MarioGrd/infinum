﻿namespace Infinum.Assignment.Infrastructure.SignalR.Model
{
    using System;
    using System.Collections.Generic;

    public class ContactDto
    {
        public ContactDto()
        {
        }

        public ContactDto(
            int id,
            string name,
            string address,
            DateTimeOffset dateOfBirth,
            List<NumberDto> numbers)
        {
            Id = id;
            Name = name;
            Address = address;
            DateOfBirth = dateOfBirth;
            Numbers = numbers;
        }

        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Address { get; set; } = string.Empty;

        public DateTimeOffset DateOfBirth { get; set; }

        public List<NumberDto> Numbers { get; set; } = new List<NumberDto>();
    }
}
