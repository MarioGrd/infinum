﻿namespace Infinum.Assignment.Infrastructure.SignalR.Model
{
    public class NumberDto
    {
        public NumberDto()
        {
        }

        public NumberDto(
            int id,
            string number)
        {
            Id = id;
            Number = number;
        }

        public int Id { get; set; }

        public string Number { get; set; } = string.Empty;
    }
}
