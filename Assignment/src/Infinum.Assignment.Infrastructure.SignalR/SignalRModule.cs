﻿namespace Infinum.Assignment.Infrastructure.SignalR
{
    using Infinum.Assignment.Port.SignalR;

    using Microsoft.Extensions.DependencyInjection;

    public static class SignalRModule
    {
        public static IServiceCollection AddSignalRModule(
            this IServiceCollection services)
        {
            _ = services.AddScoped<IBroadcastService, BroadcastService>();

            return services;
        }
    }
}
