﻿namespace Infinum.Assignment.Infrastructure.SignalR
{
    using System.Linq;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Infrastructure.SignalR.Model;
    using Infinum.Assignment.Port.SignalR;

    using Microsoft.AspNetCore.SignalR;

    using Newtonsoft.Json;

    internal class BroadcastService : IBroadcastService
    {
        private const string ContactChangedEndpoint = "contactChanged";
        private readonly IHubContext<ContactHub> hub;

        public BroadcastService(IHubContext<ContactHub> hub)
        {
            this.hub = hub;
        }

        public async Task Send(Contact contact)
        {
            ContactDto dto = new ContactDto(
                contact.Id,
                contact.Name,
                contact.Address,
                contact.DateOfBirth,
                contact.PhoneNumbers.Select(s => new NumberDto(s.Id, s.Number)).ToList());

            string message = JsonConvert.SerializeObject(dto);

            await this.hub.Clients.All.SendAsync(ContactChangedEndpoint, message);
        }
    }
}
