namespace Infinum.Assignment.API
{
    using System;
    using System.IO;
    using System.Reflection;

    using Infinum.Assignment.API.Middlewares;
    using Infinum.Assignment.Application;
    using Infinum.Assignment.Infrastructure.Database;
    using Infinum.Assignment.Infrastructure.SignalR;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationModule();
            services.AddSignalRModule();
            services
                .AddSignalR()
                .AddJsonProtocol();
            services.AddDatabaseModule(this.Configuration);

            services.AddControllers();

            services.AddSwaggerGen(config =>
            {
                config.IncludeXmlComments(
                    Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionMiddleware();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Infinum");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ContactHub>("/Contact");
            });

            app.UseCors();
        }
    }
}
