﻿namespace Infinum.Assignment.API.Controllers.Models.Contacts
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Request for creating new contact.
    /// </summary>
    public class CreateContactRequest
    {
        /// <summary>
        /// Contact name.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Contact address.
        /// </summary>
        public string Address { get; set; } = string.Empty;

        /// <summary>
        /// Contact date of birth.
        /// </summary>
        public DateTimeOffset DateOfBirth { get; set; }

        /// <summary>
        /// List of contact phone numbers.
        /// </summary>
        public List<CreatePhoneNumberRequest> PhoneNumbers { get; set; } = new List<CreatePhoneNumberRequest>();
    }
}
