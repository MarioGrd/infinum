﻿namespace Infinum.Assignment.API.Controllers.Models.Contacts
{
    /// <summary>
    /// Model used for creating phone number when we are adding new contact.
    /// </summary>
    public class CreatePhoneNumberRequest
    {
        /// <summary>
        /// Country code part of phone number aligned with E.164 specification.
        /// <remarks>
        ///
        ///     For example, if your number is +385 44 549 896, then 385 is country code.
        ///
        /// </remarks>
        /// </summary>
        public string CountryCode { get; set; } = string.Empty;

        /// <summary>
        /// National Significant Number part of phone number aligned with E.164 specification.
        /// <remarks>
        ///
        ///     For example, if your number is +385 44 549 896, then 44549896 is national significant number.
        ///
        /// </remarks>
        /// </summary>
        public string NationalSignificantNumber { get; set; } = string.Empty;
    }
}
