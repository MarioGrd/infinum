﻿namespace Infinum.Assignment.API.Controllers.Models.Contacts
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Model used for updating values.
    /// </summary>
    public class UpdateContactRequest
    {
        /// <summary>
        /// Will try to update existing contact name if different value is provided.
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Will try to update existing contact address if different value is provided.
        /// </summary>
        public string Address { get; set; } = string.Empty;

        /// <summary>
        /// Updates date of birth of a contact.
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Phone numbers you wish to add to contact.
        /// </summary>
        public List<AddPhoneNumberRequest> ToAdd { get; set; } = new List<AddPhoneNumberRequest>();

        /// <summary>
        /// Phone numbers you wish to remove from contact.
        /// </summary>
        public List<AddPhoneNumberRequest> ToDelete { get; set; } = new List<AddPhoneNumberRequest>();

        /// <summary>
        /// Existing phone numbers you wish to update on contact.
        /// </summary>
        public List<UpdatePhoneNumberRequest> ToUpdate { get; set; } = new List<UpdatePhoneNumberRequest>();
    }
}
