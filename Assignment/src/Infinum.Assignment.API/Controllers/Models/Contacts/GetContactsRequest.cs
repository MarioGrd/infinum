﻿namespace Infinum.Assignment.API.Controllers.Models.Contacts
{
    /// <summary>
    /// Get contacts request.
    /// </summary>
    public class GetContactsRequest
    {
        /// <summary>
        /// Optional name of contact.
        /// When provided, will check if there is a contact which name starts with provided value
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Optional address of contact.
        /// When provided, will check if there is a contact which address starts with provided value
        /// </summary>
        public string Address { get; set; } = string.Empty;

        /// <summary>
        /// Optional page number.
        /// If not provided, will start at 1.
        /// </summary>
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Optional page size.
        /// If not provided, will take 20 elements.
        /// </summary>
        public int PageSize { get; set; } = 20;
    }
}
