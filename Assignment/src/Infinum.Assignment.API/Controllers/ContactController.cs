﻿namespace Infinum.Assignment.API.Controllers
{
    using System.Linq;
    using System.Threading.Tasks;

    using Infinum.Assignment.API.Controllers.Models.Contacts;
    using Infinum.Assignment.Application.ContactAggregate.Command.AddPhoneNumber;
    using Infinum.Assignment.Application.ContactAggregate.Command.CreateContact;
    using Infinum.Assignment.Application.ContactAggregate.Command.DeleteContact;
    using Infinum.Assignment.Application.ContactAggregate.Command.RemovePhoneNumber;
    using Infinum.Assignment.Application.ContactAggregate.Command.UpdateContact;
    using Infinum.Assignment.Application.ContactAggregate.Command.UpdatePhoneNumber;
    using Infinum.Assignment.Application.ContactAggregate.Query.GetContact;
    using Infinum.Assignment.Application.ContactAggregate.Query.GetContacts;

    using MediatR;

    using Microsoft.AspNetCore.Mvc;

    public class ContactController : BaseController
    {
        public ContactController(IMediator mediator) : base(mediator)
        {
        }

        /// <summary>
        /// Creates new contact.
        /// </summary>
        /// <param name="request"></param>
        /// <response code="204">Indicates that operation was successful.</response>
        /// <response code="400">Indicates that an validation error occurred.</response>
        /// <response code="500">Indicates that an internal server error occurred.</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpPost]
        public async Task<IActionResult> CreateContactAsync(CreateContactRequest request)
            => await this.ProcessAsync(
                new CreateContactCommand(
                    request.Name,
                    request.Address,
                    request.DateOfBirth,
                    request.PhoneNumbers
                    .Select(pn => new CreateContactCommandPhoneNumber(pn.CountryCode, pn.NationalSignificantNumber))
                    .ToList()));

        /// <summary>
        /// Gets paginated list of contacts.
        /// </summary>
        /// <param name="request">Query parameters.</param>
        /// <response code="20o">Indicates that operation was successful.</response>
        /// <response code="400">Indicates that an validation error occurred.</response>
        /// <response code="500">Indicates that an internal server error occurred.</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpGet]
        public async Task<IActionResult> GetContactsAsync([FromQuery] GetContactsRequest request)
            => await this.ProcessAsync(
                new GetContactsQuery(
                    request.Name,
                    request.Address,
                    request.PageNumber,
                    request.PageSize));

        /// <summary>
        /// Gets single contact by his unique identifier.
        /// </summary>
        /// <param name="id">Unique identifier of a contact.</param>
        /// <response code="200">Indicates that operation was successful.</response>
        /// <response code="400">Indicates that an validation error occurred.</response>
        /// <response code="404">Indicates that entity was not found.</response>
        /// <response code="500">Indicates that an internal server error occurred.</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContactAsync(int id)
            => await this.ProcessAsync(
                new GetContactQuery(id));

        /// <summary>
        /// Updates contact by unique identifier.
        /// </summary>
        /// <param name="id">Unique identifier of a contact.</param>
        /// <param name="request">Request which will update contact.</param>
        /// <response code="204">Indicates that operation was successful.</response>
        /// <response code="400">Indicates that an validation error occurred.</response>
        /// <response code="404">Indicates that entity was not found.</response>
        /// <response code="500">Indicates that an internal server error occurred.</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, UpdateContactRequest request)
            => await this.ProcessAsync(
                new UpdateContactCommand(
                    id: id,
                    name: request.Name,
                    address: request.Address,
                    dateOfBirth: request.DateOfBirth,
                    newPhoneNumbers: request.ToAdd.Select(pn => new UpdateContactCommandPhoneNumber(pn.CountryCode, pn.NationalSignificantNumber)).ToList(),
                    updatePhoneNumbers: request.ToUpdate.Select(pn => new UpdateContactCommandToUpdatePhoneNumber(pn.CountryCode, pn.NationalSignificantNumber, pn.OldCountryCode, pn.OldNationalSignificantNumber)).ToList(),
                    deletePhoneNumbers: request.ToDelete.Select(pn => new UpdateContactCommandPhoneNumber(pn.CountryCode, pn.NationalSignificantNumber)).ToList()
                ));

        /// <summary>
        /// Adds new phone number to a contact.
        /// </summary>
        /// <param name="id">Unique identifier of a contact.</param>
        /// <param name="request">New phone number.</param>
        /// <response code="204">Indicates that operation was successful.</response>
        /// <response code="400">Indicates that an validation error occurred.</response>
        /// <response code="404">Indicates that entity was not found.</response>
        /// <response code="500">Indicates that an internal server error occurred.</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("{id}/number")]
        public async Task<IActionResult> AddPhoneNumberAsync(int id, AddPhoneNumberRequest request)
            => await this.ProcessAsync(
                new AddPhoneNumberCommand(
                    id,
                    request.CountryCode,
                    request.NationalSignificantNumber));

        /// <summary>
        /// Updates existing phone number of a contact.
        /// </summary>
        /// <param name="id">Unique identifier of a contact.</param>
        /// <param name="request">New phone number.</param>
        /// <response code="204">Indicates that operation was successful.</response>
        /// <response code="400">Indicates that an validation error occurred.</response>
        /// <response code="404">Indicates that entity was not found.</response>
        /// <response code="500">Indicates that an internal server error occurred.</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPut("{id}/number")]
        public async Task<IActionResult> UpdatePhoneNumberAsync(int id, UpdatePhoneNumberRequest request)
            => await this.ProcessAsync(
                new UpdatePhoneNumberCommand(
                    id,
                    request.CountryCode,
                    request.NationalSignificantNumber,
                    request.OldCountryCode,
                    request.OldNationalSignificantNumber));

        /// <summary>
        /// Removes phone number from a contact.
        /// </summary>
        /// <param name="id">Unique identifier of a contact.</param>
        /// <param name="request">Phone number to be removed.</param>
        /// <response code="204">Indicates that operation was successful.</response>
        /// <response code="400">Indicates that an validation error occurred.</response>
        /// <response code="404">Indicates that entity was not found.</response>
        /// <response code="500">Indicates that an internal server error occurred.</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpDelete("{id}/number")]
        public async Task<IActionResult> RemovePhoneNumberAsync(int id, RemovePhoneNumberRequest request)
            => await this.ProcessAsync(
                new RemovePhoneNumberCommand(
                    id,
                    request.CountryCode,
                    request.NationalSignificantNumber));

        /// <summary>
        /// Deletes contact and all of its phone numbers..
        /// </summary>
        /// <param name="id">Unique identifier of a contact.</param>
        /// <response code="204">Indicates that operation was successful.</response>
        /// <response code="400">Indicates that an validation error occurred.</response>
        /// <response code="404">Indicates that entity was not found.</response>
        /// <response code="500">Indicates that an internal server error occurred.</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
            => await this.ProcessAsync(
                new DeleteContactCommand(id));
    }
}
