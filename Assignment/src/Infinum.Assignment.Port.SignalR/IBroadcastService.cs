﻿namespace Infinum.Assignment.Port.SignalR
{
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;

    public interface IBroadcastService
    {
        Task Send(Contact contact);
    }
}
