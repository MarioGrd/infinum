﻿namespace Infinum.Assignment.Infrastructure.Database
{
    using System;
    using System.Reflection;

    using Infinum.Assignment.Infrastructure.Database.Core;
    using Infinum.Assignment.Infrastructure.Database.Repository;
    using Infinum.Assignment.Port.Database;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class DatabaseModule
    {
        public static IServiceCollection AddDatabaseModule(
          this IServiceCollection services,
          IConfiguration configuration)
        {
            DatabaseSettings? settings = configuration.GetSection("DatabaseSettings").Get<DatabaseSettings>();

            if (settings == null)
            {
                throw new ApplicationException($"Unable to find '{nameof(DatabaseSettings)}' settings.");
            }

            _ = services.AddDbContext<InfinummContext>(options => options.UseNpgsql(
                    settings.ConnectionString,
                    config => config.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName))
                );

            _ = services.AddScoped<IContactRepository, ContactRepository>();
            _ = services.AddScoped<IPhoneNumberRepository, PhoneNumberRepository>();
            _ = services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }

    public class DatabaseSettings
    {
        public string ConnectionString { get; set; } = string.Empty;
    }
}
