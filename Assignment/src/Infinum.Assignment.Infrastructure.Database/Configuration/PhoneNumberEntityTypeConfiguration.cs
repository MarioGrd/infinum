﻿namespace Infinum.Assignment.Infrastructure.Database.Configuration
{
    using Infinum.Assignment.Domain.ContactAggregate;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class PhoneNumberEntityTypeConfiguration : IEntityTypeConfiguration<PhoneNumber>
    {
        public void Configure(EntityTypeBuilder<PhoneNumber> builder)
        {
            builder
                .ToTable("PhoneNumbers");

            builder
                .HasKey(k => k.Id);

            builder
                .HasIndex(p => p.Number)
                .IsUnique();
        }
    }
}
