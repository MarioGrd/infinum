﻿namespace Infinum.Assignment.Infrastructure.Database.Configuration
{
    using Infinum.Assignment.Domain.ContactAggregate;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class ContactEntityTypeConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder
                .ToTable("Contacts");

            builder.Property(p => p.Name).IsRequired();

            builder.Property(p => p.Address).IsRequired();

            builder
                .Property(p => p.DateOfBirth)
                .IsRequired();

            builder
                .HasMany(p => p.PhoneNumbers)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
