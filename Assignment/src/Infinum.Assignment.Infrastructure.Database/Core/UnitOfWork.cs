﻿namespace Infinum.Assignment.Infrastructure.Database.Core
{
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Port.Database;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly InfinummContext context;

        public UnitOfWork(InfinummContext context)
        {
            this.context = context;
        }

        public async Task SaveChangesAsync(CancellationToken token = default)
        {
            await this.context.SaveChangesAsync(token);
        }
    }
}
