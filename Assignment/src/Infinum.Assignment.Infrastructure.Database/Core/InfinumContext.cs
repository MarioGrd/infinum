﻿namespace Infinum.Assignment.Infrastructure.Database.Core
{
    using System.Reflection;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;

    public class InfinummContext : DbContext
    {
        public InfinummContext(DbContextOptions<InfinummContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
            => modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        // TODO: See what can be done about this.
        public class TeamDbContextDesingFactory : IDesignTimeDbContextFactory<InfinummContext>
        {
            public InfinummContext CreateDbContext(string[] args)
            {
                DbContextOptionsBuilder<InfinummContext> optionsBuilder = new DbContextOptionsBuilder<InfinummContext>();

                optionsBuilder
                    .UseNpgsql("Server=localhost;port=5432;user id=postgres; password=postgres; database=Infinum;pooling=true");

                return new InfinummContext(optionsBuilder.Options);
            }
        }
    }
}
