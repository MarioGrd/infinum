﻿namespace Infinum.Assignment.Infrastructure.Database.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Infrastructure.Database.Core;
    using Infinum.Assignment.Port.Database;

    using Microsoft.EntityFrameworkCore;

    internal class PhoneNumberRepository : IPhoneNumberRepository
    {
        private readonly DbSet<PhoneNumber> phoneNumbers;

        public PhoneNumberRepository(InfinummContext context)
        {
            this.phoneNumbers = context.Set<PhoneNumber>();
        }

        public async Task<List<PhoneNumber>> GetPhoneNumbers(List<string> numbers)
        {
            List<PhoneNumber> items =
                await this.phoneNumbers
                .AsNoTracking()
                .Where(p => numbers.Contains(p.Number))
                .ToListAsync();

            return items.Count == 0 ? new List<PhoneNumber>() : items;
        }

        public async Task<PhoneNumber?> FindPhoneNumberByNumberAsync(string number)
        {
            return await this.phoneNumbers
                    .AsNoTracking()
                    .Where(pn => pn.Number == number)
                    .SingleOrDefaultAsync();
        }
    }
}
