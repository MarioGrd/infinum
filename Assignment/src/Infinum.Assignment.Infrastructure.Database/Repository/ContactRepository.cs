﻿namespace Infinum.Assignment.Infrastructure.Database.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Infrastructure.Database.Core;
    using Infinum.Assignment.Port.Database;

    using Microsoft.EntityFrameworkCore;

    internal class ContactRepository : IContactRepository
    {
        private readonly DbSet<Contact> contacts;

        public ContactRepository(InfinummContext context)
        {
            this.contacts = context.Set<Contact>();
        }

        public async Task<Contact?> FindContactByIdAsync(int contactId)
        {
            return await this.contacts
                .Include(i => i.PhoneNumbers)
                .Where(i => i.Id == contactId)
                .SingleOrDefaultAsync();
        }

        public async Task<Contact> FindContactByIdSafeAsync(int contactId)
        {
            Contact? contact = await this.FindContactByIdAsync(contactId);

            if (contact == null)
            {
                throw new NotFoundException("Contact not found.");
            }

            return contact;
        }

        public async Task<Contact?> FindContactByNameAndAddressAsync(
            string name,
            string address)
        {
            return await this.contacts
                .Where(contact => contact.Name.Equals(name) && contact.Address.Equals(address))
                .SingleOrDefaultAsync();
        }

        public async Task<List<Contact>> FindContactsAsync(
            int skip,
            int take,
            string? name,
            string? address)
        {
            return await this.contacts
                .AsNoTracking()
                .Include(i => i.PhoneNumbers)
                .WhereIf(!string.IsNullOrEmpty(name), c => c.Name.StartsWith(name ?? string.Empty))
                .WhereIf(!(address is null), c => c.Address.StartsWith(address ?? string.Empty))
                .OrderBy(ob => ob.Name)
                .Skip(skip)
                .Take(take)
                .ToListAsync();
        }

        public Contact AddContact(Contact contact)
        {
            this.contacts.Add(contact);

            return contact;
        }

        public Contact UpdateContact(Contact contact)
        {
            this.contacts.Update(contact);

            return contact;
        }

        public void DeleteContact(Contact contact)
        {
            this.contacts.Remove(contact);
        }
    }
}
