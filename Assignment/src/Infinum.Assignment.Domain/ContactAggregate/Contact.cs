﻿namespace Infinum.Assignment.Domain.ContactAggregate
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Contact
    {
        /// <summary>
        /// Constructor used for entity framework mapping.
        /// </summary>
        protected Contact()
        {
        }

        public Contact(
            string name,
            string address,
            DateTimeOffset dateOfBirth)
        {
            this.Name = name;
            this.Address = address;
            this.DateOfBirth = dateOfBirth;
        }

        public int Id { get; protected set; }

        public string Name { get; protected set; } = string.Empty;

        public string Address { get; protected set; } = string.Empty;

        public DateTimeOffset DateOfBirth { get; protected set; }

        public ICollection<PhoneNumber> PhoneNumbers { get; protected set; } = new List<PhoneNumber>();

        public void ChangeIdentitiy(string name, string address)
        {
            this.Name = name;
            this.Address = address;
        }

        public void ChangeDateOfBirth(DateTimeOffset dateOfBirth)
        {
            this.DateOfBirth = dateOfBirth;
        }

        public void AddPhoneNumber(PhoneNumber pn)
        {
            PhoneNumber? phoneNumber = this.GetPhoneNumber(pn.Number);

            if (phoneNumber == null)
            {
                this.PhoneNumbers.Add(pn);
            }
        }

        public void RemovePhoneNumber(string number)
        {
            PhoneNumber? phoneNumber = this.GetPhoneNumber(number);

            if (phoneNumber != null)
            {
                this.PhoneNumbers.Remove(phoneNumber);
            }
        }

        public void UpdatePhoneNumber(PhoneNumber phone, PhoneNumber oldPhoneNumber)
        {
            PhoneNumber? phoneNumber = this.GetPhoneNumber(oldPhoneNumber.Number);

            phoneNumber?.ChangePhoneNumber(phone.Number);
        }

        private PhoneNumber? GetPhoneNumber(string number)
            => this.PhoneNumbers.Where(n => n.Number == number).SingleOrDefault();
    }
}
