﻿namespace Infinum.Assignment.Domain.ContactAggregate
{
    public class PhoneNumber
    {
        protected PhoneNumber()
        {
        }

        public PhoneNumber(
            string countryCode,
            string nationalSignificantNumber)
        {
            this.Number = countryCode + nationalSignificantNumber;
        }

        public int Id { get; protected set; }

        public string Number { get; protected set; } = string.Empty;

        public void ChangePhoneNumber(string number)
        {
            this.Number = number;
        }
    }
}
