﻿namespace Infinum.Assignment.Port.Database
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IUnitOfWork
    {
        Task SaveChangesAsync(CancellationToken token = default);
    }
}
