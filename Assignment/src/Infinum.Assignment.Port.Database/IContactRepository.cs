﻿namespace Infinum.Assignment.Port.Database
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;

    public interface IContactRepository
    {
        Task<Contact?> FindContactByNameAndAddressAsync(string name, string address);

        Task<Contact?> FindContactByIdAsync(int contactId);

        Task<Contact> FindContactByIdSafeAsync(int contactId);

        Task<List<Contact>> FindContactsAsync(int skip, int take, string? name, string? address);

        Contact AddContact(Contact contact);

        Contact UpdateContact(Contact contact);

        void DeleteContact(Contact contact);
    }
}
