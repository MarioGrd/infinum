﻿namespace Infinum.Assignment.Port.Database
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;

    public interface IPhoneNumberRepository
    {
        Task<List<PhoneNumber>> GetPhoneNumbers(List<string> numbers);

        Task<PhoneNumber?> FindPhoneNumberByNumberAsync(string number);
    }
}
