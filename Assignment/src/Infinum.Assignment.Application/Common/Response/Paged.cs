﻿namespace Infinum.Assignment.Application.Common.Response
{
    using System.Collections.Generic;

    public class Paged<T> where T : class
    {
        public Paged(List<T> data, int pageNumber, int pageSize)
        {
            this.Data = data;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
        }

        public List<T> Data { get; }

        public int PageNumber { get; }

        public int PageSize { get; }
    }
}
