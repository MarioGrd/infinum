﻿namespace Infinum.Assignment.Application.Common.Query
{
    using MediatR;

    public abstract class PagedQuery<T> : IRequest<T>
    {
        private const int DefaultPageIndex = 1;
        private const int DefaultPageSize = 20;

        protected PagedQuery(
            int? pageNumber,
            int? pageSize)
        {
            this.PageNumber = !pageNumber.HasValue || pageNumber.Value < DefaultPageIndex ? DefaultPageIndex : pageNumber.Value;
            this.PageSize = !pageSize.HasValue || pageSize.Value < 0 ? DefaultPageSize : pageSize.Value;
        }

        public int PageNumber { get; }

        public int PageSize { get; }

        public int Skip
        {
            get => (this.PageNumber - 1) * this.PageSize;
        }
    }
}
