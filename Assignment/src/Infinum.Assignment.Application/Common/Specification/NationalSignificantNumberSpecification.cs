﻿namespace Infinum.Assignment.Domain.ContactAggregate
{
    using System.Linq;

    public static class NationalSignificantNumberSpecification
    {
        private const int NationalIdentificationNumberMinLength = 1;

        public static bool IsSatisfiedBy(string nationalIdentificationNumber)
        {
            return nationalIdentificationNumber.All(c => char.IsDigit(c))
                && nationalIdentificationNumber.Length >= NationalIdentificationNumberMinLength;
        }
    }
}
