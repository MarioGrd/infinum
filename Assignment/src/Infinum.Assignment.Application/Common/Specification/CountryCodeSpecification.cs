﻿namespace Infinum.Assignment.Domain.ContactAggregate
{
    using System.Linq;

    public static class CountryCodeSpecification
    {
        private const int CountryCodeMinLength = 1;
        private const int CountryCodeMaxLength = 3;

        public static bool IsSatisfiedBy(string countryCode)
        {
            return countryCode.All(c => char.IsDigit(c))
                && countryCode.Length >= CountryCodeMinLength
                && countryCode.Length <= CountryCodeMaxLength;
        }
    }
}
