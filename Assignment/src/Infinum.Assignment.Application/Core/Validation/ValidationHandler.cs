﻿namespace Infinum.Assignment.Application.Core.Validation
{
    using System;
    using System.Collections.Generic;

    using FluentValidation;
    using FluentValidation.Results;

    using Infinum.Assignment.Port.Database;

    using Newtonsoft.Json;

    public static class ValidationHandler
    {
        public static (int code, string error) Handle(Exception exception)
        {
            if (exception is NotFoundException)
            {
                return (404, "Not found.");
            }

            if (exception is ValidationException validationException)
            {
                Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();

                foreach (ValidationFailure error in validationException.Errors)
                {
                    if (dictionary.ContainsKey(error.PropertyName))
                    {
                        dictionary[error.PropertyName].Add(error.ErrorMessage);
                    }
                    else
                    {
                        dictionary.Add(error.PropertyName, new List<string>() { error.ErrorMessage });
                    }
                }

                return (400, JsonConvert.SerializeObject(dictionary));
            }

            return (500, JsonConvert.SerializeObject(new { error = "An unexpected error occurred." }));
        }
    }
}
