﻿namespace Infinum.Assignment.Application
{
    using System.Reflection;

    using FluentValidation;

    using Infinum.Assignment.Application.Core.Validation;

    using MediatR;

    using Microsoft.Extensions.DependencyInjection;

    public static class ApplicationModule
    {
        public static IServiceCollection AddApplicationModule(
            this IServiceCollection services)
        {
            _ = services.AddMediatR(typeof(ValidatorBehavior<,>).Assembly);
            _ = services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));
            _ = services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            _ = services.AddScoped<IValidatorFactory, ValidatorFactory>();

            return services;
        }
    }
}
