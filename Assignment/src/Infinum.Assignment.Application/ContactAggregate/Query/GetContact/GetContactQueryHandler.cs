﻿namespace Infinum.Assignment.Application.ContactAggregate.Query.GetContact
{
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Application.ContactAggregate.Response;
    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;

    using MediatR;

    internal class GetContactQueryHandler : IRequestHandler<GetContactQuery, ContactResponse>
    {
        private readonly IContactRepository contactRepository;
        public GetContactQueryHandler(
            IContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        public async Task<ContactResponse> Handle(GetContactQuery request, CancellationToken cancellationToken)
        {
            Contact contact = await this.contactRepository.FindContactByIdSafeAsync(request.ContactId);

            return ContactResponse.From(contact);
        }
    }
}
