﻿namespace Infinum.Assignment.Application.ContactAggregate.Query.GetContact
{
    using FluentValidation;

    public class GetContactQueryValidator : AbstractValidator<GetContactQuery>
    {
        public GetContactQueryValidator()
        {
            this.RuleFor(query => query.ContactId)
                .GreaterThan(0);
        }
    }
}
