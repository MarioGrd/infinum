﻿namespace Infinum.Assignment.Application.ContactAggregate.Query.GetContact
{
    using System;

    using Infinum.Assignment.Application.ContactAggregate.Response;

    using MediatR;

    public class GetContactQuery : IRequest<ContactResponse>
    {
        public GetContactQuery(int contactId)
        {
            ContactId = contactId;
        }

        public int ContactId { get; }
    }
}
