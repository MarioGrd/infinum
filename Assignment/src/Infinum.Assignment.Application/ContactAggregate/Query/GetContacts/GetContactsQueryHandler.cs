﻿namespace Infinum.Assignment.Application.ContactAggregate.Query.GetContacts
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Application.Common.Response;
    using Infinum.Assignment.Application.ContactAggregate.Response;
    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;

    using MediatR;

    internal class GetContactsQueryHandler : IRequestHandler<GetContactsQuery, Paged<ContactResponse>>
    {
        private readonly IContactRepository contactRepository;

        public GetContactsQueryHandler(
            IContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        public async Task<Paged<ContactResponse>> Handle(GetContactsQuery request, CancellationToken cancellationToken)
        {
            List<Contact> contacts = await this.contactRepository.FindContactsAsync(
                request.Skip,
                request.PageSize,
                request.Name,
                request.Address);

            List<ContactResponse> responses = new List<ContactResponse>();

            contacts.ForEach(contact => responses.Add(ContactResponse.From(contact)));

            return new Paged<ContactResponse>(
                responses,
                request.PageNumber,
                request.PageSize);
        }
    }
}
