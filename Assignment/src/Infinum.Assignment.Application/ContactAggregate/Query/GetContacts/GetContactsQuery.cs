﻿namespace Infinum.Assignment.Application.ContactAggregate.Query.GetContacts
{
    using Infinum.Assignment.Application.Common.Query;
    using Infinum.Assignment.Application.Common.Response;
    using Infinum.Assignment.Application.ContactAggregate.Response;

    public class GetContactsQuery : PagedQuery<Paged<ContactResponse>>
    {
        public GetContactsQuery(
            string name,
            string address,
            int? pageNumber,
            int? pageSize)
            : base(pageNumber, pageSize)
        {
            Name = name;
            Address = address;
        }

        public string? Name { get; }

        public string? Address { get; }
    }
}
