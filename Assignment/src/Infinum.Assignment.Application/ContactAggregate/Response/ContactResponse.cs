﻿namespace Infinum.Assignment.Application.ContactAggregate.Response
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Infinum.Assignment.Domain.ContactAggregate;

    public class ContactResponse
    {
        public ContactResponse(
            int id,
            string name,
            string address,
            DateTimeOffset dateOfBirth,
            List<PhoneNumberResponse> phoneNumbers)
        {
            Id = id;
            Name = name;
            Address = address;
            DateOfBirth = dateOfBirth;
            PhoneNumbers = phoneNumbers;
        }

        public int Id { get; }

        public string Name { get; }

        public string Address { get; }

        public DateTimeOffset DateOfBirth { get; }

        public List<PhoneNumberResponse> PhoneNumbers { get; }

        public static ContactResponse From(Contact contact)
        {
            return new ContactResponse(
                contact.Id,
                contact.Name,
                contact.Address,
                contact.DateOfBirth,
                contact.PhoneNumbers.Select(pn => new PhoneNumberResponse(pn.Id, pn.Number)).ToList());
        }
    }
}
