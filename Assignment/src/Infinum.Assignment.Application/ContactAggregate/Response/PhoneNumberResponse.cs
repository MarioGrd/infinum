﻿namespace Infinum.Assignment.Application.ContactAggregate.Response
{
    public class PhoneNumberResponse
    {
        public PhoneNumberResponse(
            int id,
            string phoneNumber)
        {
            Id = id;
            PhoneNumber = phoneNumber;
        }

        public int Id { get; set; }

        public string PhoneNumber { get; set; }
    }
}
