﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.CreateContact
{
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;
    using Infinum.Assignment.Port.SignalR;

    using MediatR;

    internal class CreateContactCommandHandler : AsyncRequestHandler<CreateContactCommand>
    {
        private readonly IContactRepository contactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBroadcastService broadcastService;

        public CreateContactCommandHandler(
            IContactRepository contactRepository,
            IUnitOfWork unitOfWork,
            IBroadcastService signalService)
        {
            this.contactRepository = contactRepository;
            this.unitOfWork = unitOfWork;
            this.broadcastService = signalService;
        }

        protected override async Task Handle(CreateContactCommand request, CancellationToken cancellationToken)
        {
            Contact contact = new Contact(request.Name, request.Address, request.DateOfBirth);

            foreach (CreateContactCommandPhoneNumber number in request.PhoneNumbers)
            {
                contact.AddPhoneNumber(new PhoneNumber(number.CountryCode, number.NationalSignificantNumber));
            }

            this.contactRepository.AddContact(contact);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);

            await this.broadcastService.Send(contact);
        }
    }
}
