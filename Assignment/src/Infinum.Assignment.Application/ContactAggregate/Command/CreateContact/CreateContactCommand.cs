﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.CreateContact
{
    using System;
    using System.Collections.Generic;

    using MediatR;

    public class CreateContactCommand : IRequest
    {
        public CreateContactCommand(
            string name,
            string address,
            DateTimeOffset dateOfBirth,
            List<CreateContactCommandPhoneNumber> phoneNumbers)
        {
            this.Name = name;
            this.Address = address;
            this.DateOfBirth = dateOfBirth;
            this.PhoneNumbers = phoneNumbers;
        }

        public string Name { get; }

        public string Address { get; }

        public DateTimeOffset DateOfBirth { get; }

        public List<CreateContactCommandPhoneNumber> PhoneNumbers { get; }
    }

    public class CreateContactCommandPhoneNumber
    {
        public CreateContactCommandPhoneNumber(
            string countryCode,
            string nationalSignificantNumber)
        {
            this.CountryCode = countryCode;
            this.NationalSignificantNumber = nationalSignificantNumber;
        }

        public string CountryCode { get; set; }

        public string NationalSignificantNumber { get; set; }
    }
}
