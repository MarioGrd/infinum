﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.CreateContact
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FluentValidation;
    using FluentValidation.Validators;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;

    public class CreateContactCommandValidator : AbstractValidator<CreateContactCommand>
    {
        private readonly IContactRepository contactRepository;
        private readonly IPhoneNumberRepository phoneNumberRepository;

        public CreateContactCommandValidator(
            IContactRepository contactRepository,
            IPhoneNumberRepository phoneNumberRepository)
        {
            this.contactRepository = contactRepository;
            this.phoneNumberRepository = phoneNumberRepository;

            this.RuleFor(command => command.Name)
                .MaximumLength(100)
                .NotEmpty();

            this.RuleFor(command => command.Address)
                .NotEmpty()
                .MaximumLength(100);

            this.RuleFor(command => command.DateOfBirth)
                .NotEmpty();

            this.RuleForEach(command => command.PhoneNumbers)
                .ChildRules(number =>
                {
                    number
                        .RuleFor(r => r.CountryCode)
                        .Must(cc => CountryCodeSpecification.IsSatisfiedBy(cc))
                        .WithMessage("Invalid country code provided.");

                    number
                        .RuleFor(r => r.NationalSignificantNumber)
                        .Must(nsn => NationalSignificantNumberSpecification.IsSatisfiedBy(nsn))
                        .WithMessage("Invalid National Significant Number provided.");
                });

            this.RuleFor(command => command.PhoneNumbers)
                .CustomAsync(async (numbers, ctx, _) => await this.ArePhoneNumbersAvaliable(numbers, ctx));

            this.RuleFor(command => command)
                .MustAsync(async (command, _) => await this.IsContactSaved(command.Name, command.Address))
                .WithName("General")
                .WithMessage("Contact with given name and address already exists.");
        }

        private async Task<bool> IsContactSaved(string name, string address)
            => await this.contactRepository.FindContactByNameAndAddressAsync(name, address) == null;

        private async Task<bool> ArePhoneNumbersAvaliable(List<CreateContactCommandPhoneNumber> numbers, CustomContext ctx)
        {
            List<string> lookupNumbers =
                numbers
                    .Select(number => new PhoneNumber(number.CountryCode, number.NationalSignificantNumber).Number)
                    .ToList();

            List<PhoneNumber> items = await this.phoneNumberRepository.GetPhoneNumbers(lookupNumbers);

            if (items.Count > 0)
            {
                ctx.AddFailure($"Duplicate numbers: {string.Join(",", items.Select(s => s.Number).ToList())}");
            }

            return items.Count > 0;
        }
    }
}
