﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.DeleteContact
{
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;
    using Infinum.Assignment.Port.SignalR;

    using MediatR;

    internal class DeleteContactCommandHandler : AsyncRequestHandler<DeleteContactCommand>
    {
        private readonly IContactRepository contactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBroadcastService broadcastService;

        public DeleteContactCommandHandler(
            IContactRepository contactRepository,
            IUnitOfWork unitOfWork,
            IBroadcastService signalService)
        {
            this.contactRepository = contactRepository;
            this.unitOfWork = unitOfWork;
            this.broadcastService = signalService;
        }

        protected override async Task Handle(DeleteContactCommand request, CancellationToken cancellationToken)
        {
            Contact contact = await this.contactRepository.FindContactByIdSafeAsync(request.ContactId);

            this.contactRepository.DeleteContact(contact);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);

            await this.broadcastService.Send(contact);
        }
    }
}
