﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.DeleteContact
{
    using MediatR;

    public class DeleteContactCommand : IRequest
    {
        public DeleteContactCommand(int contactId)
        {
            this.ContactId = contactId;
        }

        public int ContactId { get; }
    }
}
