﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.DeleteContact
{
    using FluentValidation;

    internal class DeleteContactCommandValidator : AbstractValidator<DeleteContactCommand>
    {
        public DeleteContactCommandValidator()
        {
            this.RuleFor(command => command.ContactId)
                .GreaterThan(0);
        }
    }
}
