﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.AddPhoneNumber
{
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;
    using Infinum.Assignment.Port.SignalR;

    using MediatR;

    internal class AddPhoneNumberCommandHandler : AsyncRequestHandler<AddPhoneNumberCommand>
    {
        private readonly IContactRepository contactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBroadcastService signalService;

        public AddPhoneNumberCommandHandler(
            IContactRepository contactRepository,
            IUnitOfWork unitOfWork,
            IBroadcastService signalService)
        {
            this.contactRepository = contactRepository;
            this.unitOfWork = unitOfWork;
            this.signalService = signalService;
        }

        protected override async Task Handle(AddPhoneNumberCommand request, CancellationToken cancellationToken)
        {
            Contact contact = await this.contactRepository.FindContactByIdSafeAsync(request.Id);

            contact.AddPhoneNumber(new PhoneNumber(request.CountryCode, request.NationalSignificantNumber));

            this.contactRepository.UpdateContact(contact);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);

            await this.signalService.Send(contact);
        }
    }
}
