﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.AddPhoneNumber
{
    using MediatR;

    public class AddPhoneNumberCommand : IRequest
    {
        public AddPhoneNumberCommand(
            int id,
            string countryCode,
            string nationalSignificantNumber)
        {
            this.Id = id;
            this.CountryCode = countryCode;
            this.NationalSignificantNumber = nationalSignificantNumber;
        }

        public int Id { get; }

        public string CountryCode { get; }

        public string NationalSignificantNumber { get; }
    }
}
