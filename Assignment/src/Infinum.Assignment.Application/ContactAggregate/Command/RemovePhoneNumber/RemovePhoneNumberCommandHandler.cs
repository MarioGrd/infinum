﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.RemovePhoneNumber
{
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;
    using Infinum.Assignment.Port.SignalR;

    using MediatR;

    internal class RemovePhoneNumberCommandHandler : AsyncRequestHandler<RemovePhoneNumberCommand>
    {
        private readonly IContactRepository contactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBroadcastService brodcastService;

        public RemovePhoneNumberCommandHandler(
            IContactRepository contactRepository,
            IUnitOfWork unitOfWork,
            IBroadcastService brodcastService)
        {
            this.contactRepository = contactRepository;
            this.unitOfWork = unitOfWork;
            this.brodcastService = brodcastService;
        }

        protected override async Task Handle(RemovePhoneNumberCommand request, CancellationToken cancellationToken)
        {
            Contact contact = await this.contactRepository.FindContactByIdSafeAsync(request.Id);

            PhoneNumber phoneNumber = new PhoneNumber(request.CountryCode, request.NationalSignificantNumber);

            contact.RemovePhoneNumber(phoneNumber.Number);

            this.contactRepository.UpdateContact(contact);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);

            await this.brodcastService.Send(contact);
        }
    }
}
