﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.RemovePhoneNumber
{
    using MediatR;
    public class RemovePhoneNumberCommand : IRequest
    {
        public RemovePhoneNumberCommand(
            int id,
            string countryCode,
            string nationalSignificantNumber)
        {
            this.Id = id;
            this.CountryCode = countryCode;
            this.NationalSignificantNumber = nationalSignificantNumber;
        }

        public int Id { get; }

        public string CountryCode { get; }

        public string NationalSignificantNumber { get; }
    }
}
