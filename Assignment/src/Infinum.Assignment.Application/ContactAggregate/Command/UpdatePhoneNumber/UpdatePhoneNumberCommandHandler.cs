﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.UpdatePhoneNumber
{
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;
    using Infinum.Assignment.Port.SignalR;

    using MediatR;

    internal class UpdatePhoneNumberCommandHandler : AsyncRequestHandler<UpdatePhoneNumberCommand>
    {
        private readonly IContactRepository contactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBroadcastService signalService;

        public UpdatePhoneNumberCommandHandler(
            IContactRepository contactRepository,
            IUnitOfWork unitOfWork,
            IBroadcastService signalService)
        {
            this.contactRepository = contactRepository;
            this.unitOfWork = unitOfWork;
            this.signalService = signalService;
        }

        protected override async Task Handle(UpdatePhoneNumberCommand request, CancellationToken cancellationToken)
        {
            Contact contact = await this.contactRepository.FindContactByIdSafeAsync(request.ContactId);

            PhoneNumber number =
                new PhoneNumber(
                    request.CountryCode,
                    request.NationalSignificantNumber);

            PhoneNumber oldNumber =
                new PhoneNumber(
                    request.OldPhoneNumberCountryCode,
                    request.OldPhoneNumberNationalSignificantNumber);

            contact.UpdatePhoneNumber(number, oldNumber);

            this.contactRepository.UpdateContact(contact);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);

            await this.signalService.Send(contact);
        }
    }
}
