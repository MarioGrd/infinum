﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.UpdatePhoneNumber
{
    using System.Threading.Tasks;

    using FluentValidation;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;

    public class UpdatePhoneNumberCommandValidator : AbstractValidator<UpdatePhoneNumberCommand>
    {
        private readonly IPhoneNumberRepository phoneNumberRepository;
        public UpdatePhoneNumberCommandValidator(
            IPhoneNumberRepository phoneNumberRepository)
        {
            this.phoneNumberRepository = phoneNumberRepository;

            this.RuleFor(command => command.CountryCode)
                .Must((cc) => CountryCodeSpecification.IsSatisfiedBy(cc))
                .WithMessage("Invalid country code provided.");

            this.RuleFor(command => command.NationalSignificantNumber)
                .Must((nsn) => NationalSignificantNumberSpecification.IsSatisfiedBy(nsn))
                .WithMessage("Invalid national significant number provided.");

            this.RuleFor(command => command)
                .MustAsync(async (command, _) => await this.IsDuplicatePhoneNumber(command))
                .WithName("General")
                .WithMessage("Given phone number already exists.");
        }

        private async Task<bool> IsDuplicatePhoneNumber(UpdatePhoneNumberCommand command)
        {
            PhoneNumber phone = new PhoneNumber(command.CountryCode, command.NationalSignificantNumber);

            PhoneNumber? phoneNumber =
                await this.phoneNumberRepository
                .FindPhoneNumberByNumberAsync(phone.Number);

            return phoneNumber == null;
        }
    }
}
