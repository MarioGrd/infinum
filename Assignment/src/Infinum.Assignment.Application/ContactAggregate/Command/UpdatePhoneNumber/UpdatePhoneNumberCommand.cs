﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.UpdatePhoneNumber
{
    using MediatR;
    public class UpdatePhoneNumberCommand : IRequest
    {
        public UpdatePhoneNumberCommand(
            int contactId,
            string countryCode,
            string nationalSignificantNumber,
            string oldPhoneNumberCountryCode,
            string oldPhoneNumberNationalSignificantNumber)
        {
            this.ContactId = contactId;
            this.CountryCode = countryCode;
            this.NationalSignificantNumber = nationalSignificantNumber;
            this.OldPhoneNumberCountryCode = oldPhoneNumberCountryCode;
            this.OldPhoneNumberNationalSignificantNumber = oldPhoneNumberNationalSignificantNumber;
        }

        public int ContactId { get; }

        public string CountryCode { get; }

        public string NationalSignificantNumber { get; }

        public string OldPhoneNumberCountryCode { get; }

        public string OldPhoneNumberNationalSignificantNumber { get; }
    }
}
