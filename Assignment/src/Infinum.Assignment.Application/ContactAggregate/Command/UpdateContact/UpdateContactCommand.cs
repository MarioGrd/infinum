﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.UpdateContact
{
    using System;
    using System.Collections.Generic;

    using MediatR;

    public class UpdateContactCommand : IRequest
    {
        public UpdateContactCommand(
            int id,
            string name,
            string address,
            DateTimeOffset dateOfBirth,
            List<UpdateContactCommandPhoneNumber> newPhoneNumbers,
            List<UpdateContactCommandToUpdatePhoneNumber> updatePhoneNumbers,
            List<UpdateContactCommandPhoneNumber> deletePhoneNumbers)
        {
            this.Id = id;
            this.Name = name;
            this.Address = address;
            this.DateOfBirth = dateOfBirth;
            this.NewPhoneNumbers = newPhoneNumbers;
            this.UpdatePhoneNumbers = updatePhoneNumbers;
            this.DeletePhoneNumbers = deletePhoneNumbers;
        }

        public int Id { get; }

        public string Name { get; }

        public string Address { get; }

        public DateTimeOffset DateOfBirth { get; }

        public List<UpdateContactCommandPhoneNumber> NewPhoneNumbers { get; }

        public List<UpdateContactCommandToUpdatePhoneNumber> UpdatePhoneNumbers { get; }

        public List<UpdateContactCommandPhoneNumber> DeletePhoneNumbers { get; }
    }

    public class UpdateContactCommandPhoneNumber
    {
        public UpdateContactCommandPhoneNumber(
            string countryCode,
            string nationalSignificantNumber)
        {
            this.CountryCode = countryCode;
            this.NationalSignificantNumber = nationalSignificantNumber;
        }

        public string CountryCode { get; }

        public string NationalSignificantNumber { get; }
    }

    public class UpdateContactCommandToUpdatePhoneNumber
    {
        public UpdateContactCommandToUpdatePhoneNumber(
            string countryCode,
            string nationalSignificantNumber,
            string oldCountryCode,
            string oldNationalSignificantNumber)
        {
            this.CountryCode = countryCode;
            this.NationalSignificantNumber = nationalSignificantNumber;
            this.OldCountryCode = oldCountryCode;
            this.OldNationalSignificantNumber = oldNationalSignificantNumber;
        }

        public string CountryCode { get; }

        public string NationalSignificantNumber { get; }

        public string OldCountryCode { get; }

        public string OldNationalSignificantNumber { get; }
    }
}
