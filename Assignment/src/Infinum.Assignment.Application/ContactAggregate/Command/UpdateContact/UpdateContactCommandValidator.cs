﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.UpdateContact
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FluentValidation;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;

    public class UpdateContactCommandValidator : AbstractValidator<UpdateContactCommand>
    {
        private readonly IContactRepository contactRepository;
        private readonly IPhoneNumberRepository phoneNumberRepository;

        public UpdateContactCommandValidator(
            IContactRepository contactRepository,
            IPhoneNumberRepository phoneNumberRepository)
        {
            this.contactRepository = contactRepository;
            this.phoneNumberRepository = phoneNumberRepository;

            this.RuleFor(command => command.Name)
                .MaximumLength(100)
                .NotEmpty();

            this.RuleFor(command => command.Address)
                .NotEmpty()
                .MaximumLength(100);

            this.RuleFor(command => command.DateOfBirth)
                .NotEmpty();

            this.RuleFor(command => command)
                .MustAsync(async (command, _) => await this.IsNewIdentityAvailable(command))
                .WithName("General")
                .WithMessage("Combination of provided name and address is not available.")
                .MustAsync(async (command, _) => await this.AreValidPhoneNumbers(command))
                .WithName("General")
                .WithMessage("There are duplicate numbers in your request or some of them already exist in database.");
        }

        private async Task<bool> IsNewIdentityAvailable(UpdateContactCommand command)
        {
            Contact contact = await this.contactRepository.FindContactByIdSafeAsync(command.Id);

            bool isNewIdentityAvailable = await this.contactRepository.FindContactByNameAndAddressAsync(
                command.Name,
                command.Address) == null;

            if (isNewIdentityAvailable)
            {
                return true;
            }

            return command.Id == contact.Id && command.Name == contact.Name && command.Address == contact.Address;
        }

        private async Task<bool> AreValidPhoneNumbers(UpdateContactCommand command)
        {
            List<string> numbers = new List<string>();
            List<string> duplicateNumbers = new List<string>();
            List<string> toCheckInDatabase = new List<string>();

            command
                .NewPhoneNumbers
                .ForEach(number =>
                {
                    string phoneNumber = number.CountryCode + number.NationalSignificantNumber;
                    toCheckInDatabase.Add(phoneNumber);

                    CheckDuplicateNumber(
                        phoneNumber,
                        numbers,
                        duplicateNumbers);
                });

            command
                .UpdatePhoneNumbers
                .ForEach(number => CheckDuplicateNumber(
                    number.OldCountryCode + number.OldNationalSignificantNumber,
                    numbers,
                    duplicateNumbers));

            command
                .UpdatePhoneNumbers
                .ForEach(number =>
                {
                    string phoneNumber = number.CountryCode + number.NationalSignificantNumber;
                    toCheckInDatabase.Add(phoneNumber);

                    CheckDuplicateNumber(
                        number.CountryCode + number.NationalSignificantNumber,
                        numbers,
                        duplicateNumbers);
                });

            command
                .DeletePhoneNumbers
                .ForEach(number => CheckDuplicateNumber(
                    number.CountryCode + number.NationalSignificantNumber,
                    numbers,
                    duplicateNumbers));

            if (duplicateNumbers.Count > 0)
            {
                return false;
            }

            List<PhoneNumber> areAllNumbersAvaliable
                = await this.phoneNumberRepository.GetPhoneNumbers(toCheckInDatabase);

            return areAllNumbersAvaliable.Count == 0;

            static void CheckDuplicateNumber(string number, List<string> numbers, List<string> duplicateNumbers)
            {
                if (numbers.Contains(number))
                {
                    duplicateNumbers.Add(number);
                }
                else
                {
                    numbers.Add(number);
                }
            }
        }
    }
}
