﻿namespace Infinum.Assignment.Application.ContactAggregate.Command.UpdateContact
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Infinum.Assignment.Domain.ContactAggregate;
    using Infinum.Assignment.Port.Database;
    using Infinum.Assignment.Port.SignalR;

    using MediatR;

    internal class UpdateContactCommandHandler : AsyncRequestHandler<UpdateContactCommand>
    {
        private readonly IContactRepository contactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBroadcastService brodcastService;

        public UpdateContactCommandHandler(
            IContactRepository contactRepository,
            IUnitOfWork unitOfWork,
            IBroadcastService brodcastService)
        {
            this.contactRepository = contactRepository;
            this.unitOfWork = unitOfWork;
            this.brodcastService = brodcastService;
        }

        protected override async Task Handle(UpdateContactCommand request, CancellationToken cancellationToken)
        {
            Contact contact = await this.contactRepository.FindContactByIdSafeAsync(request.Id);

            contact.ChangeDateOfBirth(request.DateOfBirth);
            contact.ChangeIdentitiy(request.Name, request.Address);

            List<PhoneNumber> toAdd =
                request.NewPhoneNumbers
                    .Select(pn => new PhoneNumber(pn.CountryCode, pn.NationalSignificantNumber))
                    .ToList();

            toAdd.ForEach(ta => contact.AddPhoneNumber(ta));

            List<PhoneNumber> toDelete =
                request.DeletePhoneNumbers
                    .Select(pn => new PhoneNumber(pn.CountryCode, pn.NationalSignificantNumber))
                    .ToList();

            toDelete.ForEach(td => contact.RemovePhoneNumber(td.Number));

            Dictionary<PhoneNumber, PhoneNumber>? toUpdate =
                request.UpdatePhoneNumbers
                    .Select(p => new KeyValuePair<PhoneNumber, PhoneNumber>(
                        new PhoneNumber(p.OldCountryCode, p.OldNationalSignificantNumber),
                        new PhoneNumber(p.CountryCode, p.NationalSignificantNumber)))
                    .ToDictionary(p => p.Key, v => v.Value);

            foreach (KeyValuePair<PhoneNumber, PhoneNumber> entry in toUpdate)
            {
                contact.UpdatePhoneNumber(entry.Value, entry.Key);
            }

            this.contactRepository.UpdateContact(contact);

            await this.unitOfWork.SaveChangesAsync(cancellationToken);

            await this.brodcastService.Send(contact);
        }
    }
}
