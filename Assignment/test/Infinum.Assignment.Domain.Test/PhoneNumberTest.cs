﻿namespace Infinum.Assignment.Domain.Test
{
    using Infinum.Assignment.Domain.ContactAggregate;

    using NUnit.Framework;

    public class PhoneNumberTest
    {
        [Test]
        public void ConstructorShouldCreatePhoneNumber()
        {
            PhoneNumber number = this.ArrangePhoneNumber();

            Assert.AreEqual(0, number.Id);
            Assert.AreEqual("3851", number.Number);
        }

        [Test]
        public void ChangePhoneNumberShouldChangePhoneNumber()
        {
            PhoneNumber number = this.ArrangePhoneNumber();

            number.ChangePhoneNumber("3842");

            Assert.AreEqual(0, number.Id);
            Assert.AreEqual("3842", number.Number);
        }

        private PhoneNumber ArrangePhoneNumber() => new PhoneNumber("385", "1");
    }
}
