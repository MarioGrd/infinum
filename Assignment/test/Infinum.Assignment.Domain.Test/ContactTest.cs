namespace Infinum.Assignment.Domain.Test
{
    using System;
    using System.Linq;

    using Infinum.Assignment.Domain.ContactAggregate;

    using NUnit.Framework;

    public class ContactTest
    {
        [Test]
        public void ConstructorShouldCreateNewContact()
        {
            Contact contact = this.ArrangeContact();

            Assert.NotNull(contact);
            Assert.AreEqual(0, contact.Id);
            Assert.AreEqual("Name", contact.Name);
            Assert.AreEqual("Address", contact.Address);
            Assert.That(contact.DateOfBirth, Is.EqualTo(DateTimeOffset.UtcNow).Within(1).Minutes);
            Assert.AreEqual(0, contact.PhoneNumbers.Count);
        }

        [Test]
        public void ChangeIdentityShouldChangeContactIdentity()
        {
            Contact contact = this.ArrangeContact();

            contact.ChangeIdentitiy("DifferentName", "DifferentAddress");

            Assert.AreEqual("DifferentName", contact.Name);
            Assert.AreEqual("DifferentAddress", contact.Address);
        }

        [Test]
        public void ChangeDateOfBirthShouldChangeDateOfBirth()
        {
            Contact contact = this.ArrangeContact();
            DateTimeOffset offset = new DateTimeOffset().AddHours(1);

            contact.ChangeDateOfBirth(offset);

            Assert.AreEqual(offset, contact.DateOfBirth);
        }

        [Test]
        public void AddPhoneNumberShouldAddPhoneNumber()
        {
            Contact contact = this.ArrangeContact();

            contact.AddPhoneNumber(new PhoneNumber("385", "1"));

            Assert.AreEqual(1, contact.PhoneNumbers.Count);
        }

        [Test]
        public void AddPhoneNumberShouldAddSamePhoneNumber()
        {
            Contact contact = this.ArrangeContact();

            contact.AddPhoneNumber(new PhoneNumber("385", "1"));
            contact.AddPhoneNumber(new PhoneNumber("385", "1"));

            Assert.AreEqual(1, contact.PhoneNumbers.Count);
        }

        [Test]
        public void AddPhoneNumberShouldAddDifferentPhoneNumber()
        {
            Contact contact = this.ArrangeContact();

            contact.AddPhoneNumber(new PhoneNumber("385", "1"));
            contact.AddPhoneNumber(new PhoneNumber("385", "2"));

            Assert.AreEqual(2, contact.PhoneNumbers.Count);
        }

        [Test]
        public void RemovePhoneNumberShouldRemoveExistingPhoneNumber()
        {
            Contact contact = this.ArrangeContact();
            contact.AddPhoneNumber(new PhoneNumber("385", "1"));

            contact.RemovePhoneNumber("3851");

            Assert.AreEqual(0, contact.PhoneNumbers.Count);
        }

        [Test]
        public void RemovePhoneNumberShouldIgnoreNonExistingPhoneNumber()
        {
            Contact contact = this.ArrangeContact();
            contact.AddPhoneNumber(new PhoneNumber("385", "1"));

            contact.RemovePhoneNumber("3852");

            Assert.AreEqual(1, contact.PhoneNumbers.Count);
        }

        [Test]
        public void UpdatePhoneNumberShouldUpdateExistingPhoneNumber()
        {
            Contact contact = this.ArrangeContact();
            contact.AddPhoneNumber(new PhoneNumber("385", "1"));

            contact.UpdatePhoneNumber(new PhoneNumber("385", "2"), new PhoneNumber("385", "1"));

            Assert.AreEqual(1, contact.PhoneNumbers.Count);
            Assert.AreEqual("3852", contact.PhoneNumbers.First().Number);
        }

        [Test]
        public void UpdatePhoneNumberShouldIgnoreIfOldPhoneNumberDoesNotExist()
        {
            Contact contact = this.ArrangeContact();
            contact.AddPhoneNumber(new PhoneNumber("385", "1"));

            contact.UpdatePhoneNumber(new PhoneNumber("385", "2"), new PhoneNumber("385", "3"));

            Assert.AreEqual(1, contact.PhoneNumbers.Count);
            Assert.AreEqual("3851", contact.PhoneNumbers.First().Number);
        }

        public Contact ArrangeContact() => new Contact("Name", "Address", DateTimeOffset.UtcNow);
    }
}